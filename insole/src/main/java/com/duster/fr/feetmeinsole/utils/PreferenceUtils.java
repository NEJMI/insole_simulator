package com.duster.fr.feetmeinsole.utils;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.duster.fr.feetmeinsole.greendao.DaoAccess;
import com.duster.fr.feetmeinsole.greendao.DaoSession;
import com.duster.fr.feetmeinsole.greendao.Insole;
import com.duster.fr.feetmeinsole.greendao.InsoleDao;

import java.util.List;

/**
 * Created by Anas on 14/02/2016.
 */
public class PreferenceUtils {

    private static final String TAG = PreferenceUtils.class.getSimpleName();

    /**
     * The MAC address of the default insoles.
     */
    public static final String ADDRESS_INSOLE = "address_insole";

    /**
     * The MAC address of the default insole.
     */
    private static final String ADDRESS = "address";

    /**
     * The side of the default insole.
     */
    private static final String SIDE = "side";

    /**
     * The size of the default insole.
     */
    private static final String SIZE = "size";

    /**
     * The size of the default insole.
     */
    private static final String NAME_NUMBER = "name_number";

    /**
     * The id in the db of the default insole.
     */
    private static final String INSOLE_ID = "insole_id";

    /**
     * The default data frequency.
     */
    private static final String DATA_FREQUENCY = "data_frequency";

    /**
     * The default data type.
     */
    private static final String DATA_TYPE = "data_type";

    /**
     * The default data type.
     */
    private static final String DISCOVERABLITY = "discoverability";


//    public static void setSide(Context context, int side){
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putInt(SIDE, side);
//        editor.commit();
//    }
//
//    public static void setSize(Context context, int size){
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putInt(SIZE, size);
//        editor.commit();
//    }
//
//    public static void setNameNumber(Context context, int nameNumber){
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putInt(INSOLE_ID, nameNumber);
//        editor.commit();
//    }
//
//    public static int getNameNumber(Context context){
//        return getIntPreference(context, INSOLE_ID, 73);
//    }
//
//    public static int getSide(Context context){
//        return getIntPreference(context, SIDE, 0);
//    }
//
//    public static int getSize(Context context){
//        return getIntPreference(context, SIZE, 39);
//    }
//
//    private static int getIntPreference(Context context, String name, int defaultValue){
//        return PreferenceManager.getDefaultSharedPreferences(context).getInt(name, defaultValue);
//    }
//
//    public static Long getDefaultInsoleId(Context context) {
//        return PreferenceManager.getDefaultSharedPreferences(context).getLong(INSOLE_ID, 0);
//    }

    public static void setDefaultDiscoverablityTime(Context context, int minutes){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(DISCOVERABLITY, minutes);
        editor.apply();
    }

    public static int getDefaultDiscoverablityTime(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(DISCOVERABLITY, 0);
    }

    public static void setDefaultFrequency(Context context, int frequency){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(DATA_FREQUENCY, frequency);
        editor.apply();
    }

    public static int getDefaultFrequency(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(DATA_FREQUENCY, 0);
    }

    public static void setDefaultDataType(Context context, int type){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(DATA_TYPE, type);
        editor.apply();
    }

    public static int getDefaultDataType(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(DATA_TYPE, 0);
    }


    public static void clearInsole(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ADDRESS, null);
        editor.apply();
    }

    public static Insole getDefaultInsole(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String address = prefs.getString(ADDRESS_INSOLE, null);

        if(address == null){
            return null;
        }else{
            DaoAccess access = DaoAccess.getInstance(context);
            DaoSession daoSession = access.openSession();
            InsoleDao dao = daoSession.getInsoleDao();
            Insole insole;

            List<Insole> insoles = dao.queryBuilder()
                    .where(InsoleDao.Properties.Address.eq(address))
                    .list();
            if(insoles.size() > 0){
                insole = insoles.get(0);
            }else{
                insole = null;
            }

            access.closeSession();

            return insole;
        }
    }

    public static void setDefaultInsole(Context context, Insole insole){
        if(insole != null){

            DaoAccess access = DaoAccess.getInstance(context);
            DaoSession daoSession = access.openSession();
            InsoleDao dao = daoSession.getInsoleDao();

            List<Insole> insoles = dao.queryBuilder()
                    .where(InsoleDao.Properties.Address.eq(insole.getAddress()))
                    .list();
            if(insoles.size() > 0){
                insole = insoles.get(0);
            }else{
                dao.insert(insole);
            }
            access.closeSession();

            BluetoothAdapter.getDefaultAdapter().setName(insole.getName());

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(ADDRESS_INSOLE, insole.getAddress());
            editor.apply();
        }
    }
}
