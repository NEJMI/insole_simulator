package com.duster.fr.feetmeinsole.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.duster.fr.feetmeinsole.R;
import com.duster.fr.feetmeinsole.data.Version;
import com.duster.fr.feetmeinsole.greendao.DaoAccess;
import com.duster.fr.feetmeinsole.greendao.DaoSession;
import com.duster.fr.feetmeinsole.greendao.Insole;
import com.duster.fr.feetmeinsole.greendao.InsoleDao;
import com.duster.fr.feetmeinsole.utils.InsoleUtils;

import java.lang.ref.WeakReference;
import java.util.Random;

/**
 * Created by Anas on 01/03/2016.
 */
public class InsoleCreationView extends LinearLayout {

    private AppCompatSpinner mNumberSpinner;
    private AppCompatSpinner mSizeSpinner;
    private AppCompatCheckBox mLeftCheckBox;
    private AppCompatCheckBox mRightCheckBox;

    private TextView mNumberTitle;
    private TextView mSizeTitle;
    private TextView mSideTitle;

    private TextView mMacAddressView;
    private String mMacAddress;

    public interface Listener {
        void onAddInsole(Insole insole);
    }

    private WeakReference<Listener> mListener;

    public void setListener(Listener listener) {
        mListener = new WeakReference<>(listener);
    }

    public InsoleCreationView(Context context) {
        super(context);
        init(context);
    }

    public InsoleCreationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }



    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.layout_insole_creation, this, true);

        //--- Number ---//
        mNumberTitle = (TextView) findViewById(R.id.number_title);
        mNumberSpinner = (AppCompatSpinner) findViewById(R.id.number_spinner);
        String[] numberItems = new String[150];
        numberItems[0] = "-";
        for(int i = 1; i < numberItems.length; i++){
            numberItems[i] = String.valueOf(i);
        }
        ArrayAdapter<String> numberAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_dropdown_item_1line, numberItems);
        mNumberSpinner.setAdapter(numberAdapter);

        //--- Size ---//
        mSizeTitle = (TextView) findViewById(R.id.size_title);
        mSizeSpinner = (AppCompatSpinner) findViewById(R.id.size_spinner);
        String[] sizeItems = new String[10];
        sizeItems[0] = "-";
        for(int i=1; i<sizeItems.length; i++){
            sizeItems[i] = String.valueOf(36 + i);
        }
        ArrayAdapter<String> sizeAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, sizeItems);
        mSizeSpinner.setAdapter(sizeAdapter);

        //--- Side ---//
        mSideTitle = (TextView) findViewById(R.id.side_title);
        mLeftCheckBox = (AppCompatCheckBox) findViewById(R.id.left_checkbox);
        mRightCheckBox = (AppCompatCheckBox) findViewById(R.id.right_checkbox);

        mLeftCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mRightCheckBox.setChecked(false);
                }
            }
        });

        mRightCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mLeftCheckBox.setChecked(false);
                }
            }
        });

        mMacAddressView = (TextView) findViewById(R.id.address_title);

        AppCompatButton generateAddressBtn = (AppCompatButton) findViewById(R.id.generate_mac_address);
        generateAddressBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mMacAddress = randomMACAddress();
                mMacAddressView.setText(mMacAddress);
            }
        });
    }

    private void vibrate(TextView textView){
        textView.setTextColor(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark));
        Animation vibrateAnim = AnimationUtils.loadAnimation(getContext(), R.anim.vibrate);
        textView.startAnimation(vibrateAnim);
    }

    public boolean areValuesEnteredCorrect(){
        boolean isCorrect = true;
        if(mNumberSpinner.getSelectedItem() == mSizeSpinner.getItemAtPosition(0)){
            vibrate(mNumberTitle);
            isCorrect = false;
        }else{
            mNumberTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryText));
        }

        if(mSizeSpinner.getSelectedItem() == mSizeSpinner.getItemAtPosition(0)){
            vibrate(mSizeTitle);
            isCorrect = false;
        }else{
            mSizeTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryText));
        }

        if(!mLeftCheckBox.isChecked() && !mRightCheckBox.isChecked()){
            vibrate(mSideTitle);
            isCorrect = false;
        }else{
            mSideTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryText));
        }

        if(mMacAddress == null || isAlreadyTaken(mMacAddress)){
            vibrate(mMacAddressView);
            isCorrect = false;
        }else {
            mMacAddressView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryText));
        }

        return isCorrect;
    }

    private boolean isAlreadyTaken(String macAddress) {
        DaoAccess access = DaoAccess.getInstance(getContext());
        DaoSession daoSession = access.openSession();
        InsoleDao insoleDao = daoSession.getInsoleDao();
        Insole duplicateInsole = Insole.geInsoleWithSameAddress(insoleDao, macAddress);

        access.closeSession();

        return duplicateInsole != null;

    }

    public void createInsole(){
        Insole insole = new Insole();

        //--- Size ---//
        if(mSizeSpinner.getSelectedItemPosition() == 0){
        }else{
            insole.setSize(Integer.parseInt((String) mSizeSpinner.getSelectedItem()));
        }

        //--- Side ---//
        if(mRightCheckBox.isChecked()){
            insole.setSide(InsoleUtils.SIDE_RIGHT);
        }else if (mLeftCheckBox.isChecked()){
            insole.setSide(InsoleUtils.SIDE_LEFT);
        }

        if(mMacAddress != null){
            insole.setAddress(mMacAddress);
        }

        Version hardVersion = new Version(0, 0, 1);
        Version softVersion = new Version(2, 0, 0);

        insole.setVersion(hardVersion.toString());
        insole.setVersionCode(softVersion.toString());

        int insoleNumber = Integer.parseInt((String) mNumberSpinner.getSelectedItem());

        insole.setName(getInsoleName(insole.getSide(), insole.getSize(), String.format("%02d", insoleNumber)));

        addInsole(insole);

    }

    private void addInsole(Insole insole) {
        if (mListener != null) {
            Listener listener = mListener.get();
            if (listener != null) listener.onAddInsole(insole);
        }
    }

    private String getInsoleName(int side, int size, String number){
        if(side == InsoleUtils.SIDE_LEFT){
            return "FeetMe " + number + "L-" + size;
        }else{
            return "FeetMe " + number + "R-" + size;
        }
    }

    private String randomMACAddress(){
        Random rand = new Random();
        byte[] macAddress = new byte[6];
        rand.nextBytes(macAddress);

        macAddress[0] = (byte)(macAddress[0] & (byte)254);  //zeroing last 2 bytes to make it unicast and locally adminstrated

        StringBuilder sb = new StringBuilder(18);
        for(byte b : macAddress){

            if(sb.length() > 0)
                sb.append(":");

            sb.append(String.format("%02x", b));
        }


        return sb.toString();
    }
}
