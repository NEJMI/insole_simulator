package com.duster.fr.feetmeinsole.activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.duster.fr.feetmeinsole.R;
import com.duster.fr.feetmeinsole.fragments.HomeFragment;
import com.duster.fr.feetmeinsole.greendao.Insole;
import com.duster.fr.feetmeinsole.managers.IInsoleRegistrationManager;
import com.duster.fr.feetmeinsole.managers.InsoleRegistrationManager;
import com.duster.fr.feetmeinsole.utils.PreferenceUtils;
import com.duster.fr.feetmeinsole.utils.Utils;

/**
 * Created by Anas on 29/02/2016.
 */
public class HomeActivity extends ServiceActivity{

    public static Intent newIntent(Context packageContext){
        return new Intent(packageContext, HomeActivity.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_frame, new HomeFragment())
                .commit();

        register();
    }

    private void register() {
        Insole insole = PreferenceUtils.getDefaultInsole(HomeActivity.this);
        if(insole == null){
            new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    return Utils.checkInternetConnection(HomeActivity.this);
                }

                @Override
                protected void onPostExecute(Boolean aBoolean) {
                    if(aBoolean){
                        new InsoleRegistrationManager(getBaseContext())
                                .startRegistration();
                    }
                }
            }.execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_select_insole:
                startActivity(InsoleActivity.newIntent(this));
                return true;
            case R.id.action_settings:
                startActivity(SettingsActivity.newIntent(this));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
