package com.duster.fr.feetmeinsole.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.duster.fr.feetmeinsole.R;
import com.duster.fr.feetmeinsole.activities.HomeActivity;
import com.duster.fr.feetmeinsole.views.SettingsView;

/**
 * Created by Anas on 14/02/2016.
 */
public class SettingsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);


        final SettingsView settingsView = (SettingsView) view.findViewById(R.id.settings_view);
        final TextView mUpdate = (TextView) view.findViewById(R.id.update_settings);

        mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsView.updateSettings();
                startHomeActivity();
            }
        });
        return view;
    }

    private void startHomeActivity() {
        getActivity().startActivity(HomeActivity.newIntent(getActivity()));
        getActivity().supportFinishAfterTransition();
    }


}
