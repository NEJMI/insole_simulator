package com.duster.fr.feetmeinsole.service;

/**
 * Created by Anas on 13/02/2016.
 */
public interface IMainServiceManager {

    /**
     * Starts automatic connection to the remote devices.
     * While the automatic connection is on, classes implementing this interface will continuously
     * try to connect to remote devices that are set as default remote devices.
     */
    void startConnection();

    /**
     * Stop the automatic connection to default remote devices.
     * Disconnects remote devices if connected.
     */
    void stopConnection();

    /**
     * Returns the automatic connection state.
     * @return true if automatic connection is on, false otherwise
     */
    boolean isConnectionStarted();

    /**
     * Gives the connection state of the remote device.
     */
    boolean isConnected();
}
