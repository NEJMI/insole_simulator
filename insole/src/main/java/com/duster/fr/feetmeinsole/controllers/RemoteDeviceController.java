package com.duster.fr.feetmeinsole.controllers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.duster.fr.feetmeinsole.Build;
import com.duster.fr.feetmeinsole.greendao.Insole;
import com.duster.fr.feetmeinsole.utils.PreferenceUtils;
import com.duster.fr.feetmeinsole.utils.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Anas on 20/02/2016.
 */
public class RemoteDeviceController implements IRemoteDeviceController {

    private static final String TAG = RemoteDeviceController.class.getSimpleName();
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final String IGNORED_MSG = "#hello";
    private static final int WRITE_DELAY_MS= 500;
    private static final int MSG_STATE_CHANGE = 100;
    private static final int MSG_REMOTE_DEVICE = 101;

    private volatile IRemoteFunctionController mRFController;

    /*
    * Handler to send insole info to the Thread that created this class.
    */
    private final Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            int what = msg.what;
            if(what == RemoteFunction.PONG.code()){
                mListener.onPongRequested();
            }else if(what == RemoteFunction.SENSOR_NB.code()){
                mListener.onSensorNbRequested();
            }else if(what == RemoteFunction.VERSION.code()){
                mListener.onVersionRequested();
            }else if(what == RemoteFunction.SIDE.code()){
                mListener.onSideRequested();
            }else if(what == RemoteFunction.RESET_TIMESTAMP.code()){
                mListener.onResetTimestampRequested();
            }else if(what == RemoteFunction.BATTERY.code()){
                mListener.onBatteryRequested();
            }else if(what == RemoteFunction.SIZE.code()){
                mListener.onSizeRequested();
            }else if(what == RemoteFunction.START_SENDING.code()){
                mListener.onStartSending();
            }else if(what == RemoteFunction.STOP_SENDING.code()){
                mListener.onStopSending();
            }else if(what == RemoteFunction.CALIBRATION.code()){
                mListener.onCalibration();
            }else if(what == MSG_STATE_CHANGE){
                mListener.onStateChanged(msg.arg1);
            }else if(what == MSG_REMOTE_DEVICE){
                mListener.onRemoteDeviceConnected((BluetoothDevice)msg.obj);
            }
        }
    };

    private volatile int mState;

    private final BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();

    private AcceptThread mAcceptThread;
    private ConnectedThread mConnectedThread;

    private final Listener mListener;

    private final Insole mInsole;

    private Context mContext;

    public static IRemoteDeviceController newInstance(Context context, //used in BLE build flavor
                                                Insole insole,
                                                Listener listener){
        return new RemoteDeviceController(context, insole, listener);
    }

    private IRemoteFunctionController newRemoteFunctionsController(Context context, OutputStream outputStream){
        return RemoteFunctionController.newInstance(context, outputStream);
    }

    private RemoteDeviceController(Context context, Insole insole, Listener listener){
        mContext = context;
        mState = STATE_DISCONNECTED;
        mInsole = insole;
        mListener = listener;
    }

    private synchronized void setState(int state) {
        if(Build.DEBUG) Log.d(TAG, "setState() " + mState + " -> " + state + " for insole " + mInsole.getSide());
        mState = state;
        mHandler.obtainMessage(MSG_STATE_CHANGE, state, -1).sendToTarget();
    }

    @Override
    public int getState() {
        return mState;
    }

    @Override
    public synchronized void accept() {
        if(Build.DEBUG) Log.d(TAG, "accept");
        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_LISTEN);

        // Start the thread to listen on a BluetoothServerSocket
        if (mAcceptThread == null) {
            mAcceptThread = new AcceptThread();
            mAcceptThread.start();
        }
    }

    private void init() {
        if (mAcceptThread != null) {mAcceptThread.cancel(); mAcceptThread = null;}
        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}
    }

    @Override
    public synchronized void disconnect() {
        if(Build.DEBUG) Log.d(TAG, "disconnect");
        init();
        setState(STATE_DISCONNECTED);
    }

    @Override
    public void sendFirstFrame() {
        try {
            if(mRFController != null){
                mRFController.onConnection();
            }
        } catch (IOException e) {
            Log.e(TAG, "disconnected", e);
            disconnect();
        }
    }

    synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
        if(Build.DEBUG)  Log.d(TAG, "connected");
        if (mAcceptThread != null) {mAcceptThread.cancel(); mAcceptThread = null;}
        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket, mHandler);
        mConnectedThread.start();
        setState(STATE_CONNECTED);

        mHandler.obtainMessage(MSG_REMOTE_DEVICE, device).sendToTarget();
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            BluetoothServerSocket tmp = null;

            // Create a new listening server socket
            try {
                tmp = mAdapter.listenUsingRfcommWithServiceRecord(TAG,
                            MY_UUID);
            } catch (IOException e) {
                Log.e(TAG, "listen() failed", e);
            }
            mmServerSocket = tmp;
        }

        public void run() {
            Log.d(TAG, "BEGIN mAcceptThread" + this);
            setName("AcceptThread");

            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while (mState != STATE_CONNECTED) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    socket = mmServerSocket.accept();
                    Log.d("AcceptThread", "still waiting");
                } catch (IOException e) {
                    Log.e(TAG, "accept() failed", e);
                    break;
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized (RemoteDeviceController.this) {
                        switch (mState) {
                            case STATE_LISTEN:
                            case STATE_CONNECTING:
                                // Situation normal. Start the connected thread.
                                connected(socket, socket.getRemoteDevice());
                                break;
                            case STATE_DISCONNECTED:
                            case STATE_CONNECTED:
                                // Either not ready or already connected. Terminate new socket.
                                try {
                                    socket.close();
                                } catch (IOException e) {
                                    Log.e(TAG, "Could not close unwanted socket", e);
                                }
                                break;
                        }
                    }
                }
            }
            Log.i(TAG, "END mAcceptThread");

        }

        public void cancel() {
            Log.d(TAG, "cancel " + this);
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of server failed", e);
            }
        }
    }


    /*
     * Thread that manages sending and receiving data during Bluetooth connection.
     */
    private class ConnectedThread extends Thread {

        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private final Handler mmHandler;
        private String message;
        private int frameLength;
        private final int MAX_BUFFER_SIZE = 200;
        private byte[] buffer = new byte[MAX_BUFFER_SIZE];  // buffer store for the stream

        public ConnectedThread(BluetoothSocket socket, Handler handler) {
            mmSocket = socket;
            mmHandler = handler;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "Could not get streams from socket");
                disconnect();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {

            mRFController = newRemoteFunctionsController(mContext, mmOutStream);

            //waiting solves many issues
            //Can't wait too long before writing or the insole will stop
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                Log.w(TAG, Log.getStackTraceString(e));
            }


            // Keep listening to the InputStream until an exception occurs
            while (true){
                try {
                    interceptRemoteFunctions();
                } catch (IOException e) {
                    Log.e(TAG, "disconnected", e);
                    disconnect();
                    break;
                }
            }

        }

        /* Call this from the BTService to shutdown the connection */
        public void cancel() {
            if(mRFController != null) {
                mRFController = null;
                Log.i(TAG, "killing RFController");
            }
            try {
                Log.i(TAG, "Closing socket");
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close socket", e);
            }
        }

        private void interceptRemoteFunctions() throws IOException {
            frameLength = mmInStream.read(buffer);
            message = new String(buffer,0,frameLength);

            if("ping".equals(message)){
                if (Build.DEBUG) Log.d(TAG, "<<<<<<<--PONG REQUESTED-->>>>>>>");
                mRFController.onPingRequest();
                mmHandler.obtainMessage(RemoteFunction.PONG.code()).sendToTarget();
            }else if("sensors_number".equals(message)){
                if (Build.DEBUG) Log.d(TAG, "<<<<<<<--SENSORS NUMBER REQUESTED-->>>>>>>");
                mRFController.onSensorNbRequest();
                mmHandler.obtainMessage(RemoteFunction.SENSOR_NB.code()).sendToTarget();
            }else if("version".equals(message)){
                if (Build.DEBUG) Log.d(TAG, "<<<<<<<--VERSION REQUESTED-->>>>>>>");
                mRFController.onVersionRequest();
                mmHandler.obtainMessage(RemoteFunction.VERSION.code()).sendToTarget();
            }else if("insole_side".equals(message)){
                if (Build.DEBUG) Log.d(TAG, "<<<<<<<--INSOLE SIDE REQUESTED-->>>>>>>");
                mRFController.onSideRequest();
                mmHandler.obtainMessage(RemoteFunction.SIDE.code()).sendToTarget();
            }else if("reset_timestamp".equals(message)){
                if (Build.DEBUG) Log.d(TAG, "<<<<<<<--RESET TIMESTAMP REQUESTED-->>>>>>>");
                mRFController.onResetTimeStampRequest();
                mmHandler.obtainMessage(RemoteFunction.RESET_TIMESTAMP.code()).sendToTarget();
            }else if("battery_info".equals(message)){
                if (Build.DEBUG) Log.d(TAG, "<<<<<<<--BATTERY INFO REQUESTED-->>>>>>>");
                mRFController.onBatteryLevelRequest();
                mmHandler.obtainMessage(RemoteFunction.BATTERY.code()).sendToTarget();
            }else if("footsize".equals(message)){
                if (Build.DEBUG) Log.d(TAG, "<<<<<<<--FOOTSIZE REQUESTED-->>>>>>>");
                mRFController.onSizeRequest();
                mmHandler.obtainMessage(RemoteFunction.SIZE.code()).sendToTarget();
            }else if("start_sending".equals(message)){
                if (Build.DEBUG) Log.d(TAG, "<<<<<<<--START SENDING REQUESTED-->>>>>>>");
                mRFController.onStartSendingRequest();
                mmHandler.obtainMessage(RemoteFunction.START_SENDING.code()).sendToTarget();
            }else if("stop_sending".equals(message)){
                if (Build.DEBUG) Log.d(TAG, "<<<<<<<--STOP SENDING REQUESTED-->>>>>>>");
                mRFController.onStopSendingRequest();
                mmHandler.obtainMessage(RemoteFunction.STOP_SENDING.code()).sendToTarget();
            }else if("sensor_calibration".equals(message)){
                if (Build.DEBUG) Log.d(TAG, "<<<<<<<--CALIBRATION REQUESTED-->>>>>>>");
                mRFController.onCalibrationRequest();
                mmHandler.obtainMessage(RemoteFunction.CALIBRATION.code()).sendToTarget();
            }

        }
    }
}
