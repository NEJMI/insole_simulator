package com.duster.fr.feetmeinsole.gl;

import android.opengl.GLSurfaceView;

import com.duster.fr.feetmeinsole.greendao.Insole;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Anas on 19/02/2016.
 */
public class InsoleRenderer implements GLSurfaceView.Renderer {

    private static final String TAG = InsoleRenderer.class.getSimpleName();

    /**
     * Is false when the heatmap must not be drawn. True when the heatmap must be drawn.
     * When it is true, mInsole must not be null.
     */
    private boolean isDrawing = false;

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        //TODO
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        //TODO
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        //TODO
    }











    public void enableDrawing(Insole insole){
        //TODO
    }

    public void disableDrawing(){
        isDrawing = false;
    }
}
