package com.duster.fr.feetmeinsole.managers;

import android.content.Context;
import android.os.AsyncTask;

import com.duster.fr.feetmeinsole.data.Version;
import com.duster.fr.feetmeinsole.greendao.Insole;
import com.duster.fr.feetmeinsole.utils.InsoleUtils;
import com.duster.fr.feetmeinsole.utils.PreferenceUtils;

/**
 * Created by Anas on 29/02/2016.
 */
public class InsoleRegistrationManager implements IInsoleRegistrationManager {
    private Context mContext;

    public InsoleRegistrationManager(Context context){
        mContext = context;
    }


    private AsyncTask<Void, Void, Void> mRegistrationTask = new AsyncTask<Void, Void, Void>() {
        @Override
        protected Void doInBackground(Void... params) {

            Insole insole = InsoleUtils.newInstance("00:0A:95:9D:68:16", "FeetMe 00L-40");
            Version hardVersion = new Version(0, 0, 1);
            Version softVersion = new Version(2, 0, 0);

            insole.setVersion(hardVersion.toString());
            insole.setVersionCode(softVersion.toString());

            insole.setSensorNb(68);

            PreferenceUtils.setDefaultInsole(mContext, insole);

            //discoverability
            PreferenceUtils.setDefaultDiscoverablityTime(mContext, 0);

            return null;
        }
    };

    @Override
    public void startRegistration() {
        mRegistrationTask.execute();
    }
}
