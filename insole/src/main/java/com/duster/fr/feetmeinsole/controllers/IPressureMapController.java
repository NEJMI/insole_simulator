package com.duster.fr.feetmeinsole.controllers;

/**
 * Created by Anas on 19/02/2016.
 */
public interface IPressureMapController {

    void onResume();

    void onPause();

    void startDrawing();

    void stopDrawing();

}
