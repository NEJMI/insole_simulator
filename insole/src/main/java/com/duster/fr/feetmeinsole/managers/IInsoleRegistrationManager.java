package com.duster.fr.feetmeinsole.managers;

/**
 * Created by Anas on 29/02/2016.
 */
public interface IInsoleRegistrationManager {
    void startRegistration();

    interface Listener {

        void onRegistrationSuccess();

        void onRegistrationFailure();
    }
}
