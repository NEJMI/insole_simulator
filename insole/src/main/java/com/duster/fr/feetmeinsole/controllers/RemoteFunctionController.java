package com.duster.fr.feetmeinsole.controllers;


import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.duster.fr.feetmeinsole.greendao.Insole;
import com.duster.fr.feetmeinsole.utils.InsoleUtils;
import com.duster.fr.feetmeinsole.utils.PreferenceUtils;

import org.w3c.dom.ProcessingInstruction;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Anas on 23/02/2016.
 */
public class RemoteFunctionController implements IRemoteFunctionController {

    private Insole mDefaultInsole;
    private Context mContext;
    private byte[] mFramesToSend;
    private boolean startSending;
    private final ConcurrentLinkedQueue<RemoteFunction> mRequestsQueue;
//    private ResponseDispatcherThread mResponseDispatcherThread;

    private ResponseDispatchHandler mDispatchHandler;

    public static IRemoteFunctionController newInstance(Context context, OutputStream outputStream) {
        return new RemoteFunctionController(context, outputStream);
    }


    private RemoteFunctionController(Context context, OutputStream outputStream){
        mContext = context;
        mDefaultInsole = PreferenceUtils.getDefaultInsole(context);
        mFramesToSend = getDataFrame();
        mRequestsQueue = new ConcurrentLinkedQueue<>();

        HandlerThread handlerThread = new HandlerThread("ResponsesDispatcher");
        handlerThread.start();

        mDispatchHandler = new ResponseDispatchHandler(handlerThread.getLooper(), outputStream, mRequestsQueue);
    }

//    @Override
//    public synchronized void startResponseDispatcherThread(OutputStream outputStream) {
//        if(mResponseDispatcherThread != null){mResponseDispatcherThread.cancel(); mResponseDispatcherThread = null;}
//
//        mResponseDispatcherThread = new ResponseDispatcherThread(mRequestsQueue, outputStream);
//        mResponseDispatcherThread.start();
//
//    }
//
//    @Override
//    public synchronized void stopResponseDispatcherThread() {
//        if(mResponseDispatcherThread != null){mResponseDispatcherThread.cancel(); mResponseDispatcherThread = null;}
//    }

    @Override
    public void onConnection() throws IOException {
        addRequestToQueue(RemoteFunction.MADM);
        mDispatchHandler.sendEmptyMessageDelayed(50, 500);
    }

    @Override
    public void onPingRequest() throws IOException {
//        write(RemoteFunction.PONG);
        addRequestToQueue(RemoteFunction.PONG);
        mDispatchHandler.sendEmptyMessageDelayed(50, 500);
    }

    @Override
    public void onSensorNbRequest() throws IOException {
//        write(RemoteFunction.SENSOR_NB);
        addRequestToQueue(RemoteFunction.SENSOR_NB);
        mDispatchHandler.sendEmptyMessageDelayed(50, 500);
    }

    @Override
    public void onVersionRequest() throws IOException {
//        write(RemoteFunction.VERSION);
        addRequestToQueue(RemoteFunction.VERSION);
        mDispatchHandler.sendEmptyMessageDelayed(50, 500);
    }

    @Override
    public void onSideRequest() throws IOException {
//        write(RemoteFunction.SIDE);
        addRequestToQueue(RemoteFunction.SIDE);
        mDispatchHandler.sendEmptyMessageDelayed(50, 500);
    }

    @Override
    public void onSizeRequest() throws IOException {
//        write(RemoteFunction.SIZE);
        addRequestToQueue(RemoteFunction.SIZE);
        mDispatchHandler.sendEmptyMessageDelayed(50, 500);
    }

    @Override
    public void onBatteryLevelRequest() throws IOException {
//        write(RemoteFunction.BATTERY);
        addRequestToQueue(RemoteFunction.BATTERY);
        mDispatchHandler.sendEmptyMessageDelayed(50, 500);
        Log.d("BATTERY", "requested");
    }

    @Override
    public void onStartSendingRequest() throws IOException {
        addRequestToQueue(RemoteFunction.START_SENDING);
        mDispatchHandler.sendEmptyMessageDelayed(50, 500);
    }

    @Override
    public void onStopSendingRequest() throws IOException {
        addRequestToQueue(RemoteFunction.STOP_SENDING);
        mDispatchHandler.sendEmptyMessageDelayed(50, 500);
    }

    @Override
    public void onCalibrationRequest() throws IOException {
        //do nothing internally and respond to the request
//        write(RemoteFunction.CALIBRATION);
        addRequestToQueue(RemoteFunction.CALIBRATION);
        mDispatchHandler.sendEmptyMessageDelayed(50, 500);
    }

    @Override
    public void onResetTimeStampRequest() throws IOException {
        //do nothing internally and respond to the request
//        write(RemoteFunction.RESET_TIMESTAMP);
        addRequestToQueue(RemoteFunction.RESET_TIMESTAMP);
        mDispatchHandler.sendEmptyMessageDelayed(50, 500);
    }

    private byte[] getTimestamp(){
        int timestamp = (int)(System.currentTimeMillis() / 1000);
        int t4 = (int) (timestamp/(Math.pow(256,3)));
        int t3 = (int) ((timestamp - t4*Math.pow(256,3))/(Math.pow(256,2)));
        int t2 = (int) ((timestamp - t4*Math.pow(256,3) - t3*Math.pow(256,2))/(Math.pow(256,1)));
        int t1 = (int) (timestamp - t4*Math.pow(256,3) - t3*Math.pow(256,2) - t2*Math.pow(256,1));
        return new byte[]{(byte) t1, (byte) t2, (byte) t3, (byte) t4};
    }

    private byte[] getResponse(int code){
        switch (code){
            case 0:
                return mFramesToSend;
            case 1:
                return "pong".getBytes();
            case 2:
                int sensorNb = mDefaultInsole.getSensorNb();
                return new byte[]{(byte) sensorNb};
            case 3:
                byte[] hardVersionsBytes = mDefaultInsole.getVersion().getBytes();
                byte[] softVersionsBytes = mDefaultInsole.getVersionCode().getBytes();
                byte[] version = new byte[6];
                for( int i = 0; i<6; i++){
                    if(i <3){
                        version[i] = hardVersionsBytes[i];
                    }else{
                        version[i] = softVersionsBytes[i-3];
                    }
                }
                return version;
            case 4:
                int side = mDefaultInsole.getSide();
                if(side == InsoleUtils.SIDE_LEFT){
                    return "L".getBytes();
                }else if (side == InsoleUtils.SIDE_RIGHT){
                    return "R".getBytes();
                }
            case 6:
                byte[] battery = new byte[40];
                battery[4] = (byte)((getCurrentBatteryLevel()*10)%256);
                battery[5] = (byte)(getCurrentBatteryLevel()*10/256);
                battery[8] = 1; //voltage not null
                return battery;
            case 8:
                int size = mDefaultInsole.getSize();
                return new byte[]{(byte) size};
            case 10:
                return "start".getBytes();
            case 11:
                return "stop".getBytes();

            default: return null;
        }
    }

    private int getCurrentBatteryLevel() {
        Intent batteryIntent = mContext.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        return batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
    }



    private byte[] getDataFrame(){
        byte[] dataFrame = new byte[mDefaultInsole.getSensorNb()];

        for(int i = 0; i<dataFrame.length; i++){
            dataFrame[i] = (byte) 255;
        }

        return dataFrame;
    }

    private void addRequestToQueue(RemoteFunction remoteFunction){
        mRequestsQueue.add(remoteFunction);
    }


//    private class ResponseDispatcherThread extends Thread{
//
//        public static final String TAG = "RDThread";
//
//        private ConcurrentLinkedQueue<RemoteFunction> mmRequestsQueue;
//        private OutputStream mOutputStream;
//        private int requestsCount = 0;
//
//        public ResponseDispatcherThread(ConcurrentLinkedQueue<RemoteFunction> queue, OutputStream outputStream){
//            mmRequestsQueue = queue;
//            mOutputStream = outputStream;
//        }
//
//        @Override
//        public void run() {
//
//
//            if(mmRequestsQueue != null){
//                while (!mmRequestsQueue.isEmpty()){
//                    if(mmRequestsQueue.peek().code() == RemoteFunction.MADM.code()){
//                        try {
//                            writeFirstFrame();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }else if(mmRequestsQueue.peek().code()<RemoteFunction.IGNORED.code()){
//                        try {
//                            write(mmRequestsQueue.poll());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }else{
//                        mmRequestsQueue.poll();
//                    }
//
//                    requestsCount++;
//                    if (requestsCount == 200) {
//                        requestsCount = 0;
//                        Log.d(TAG, "--- 200 frames ---");
//                        Log.d("mmRequestsQueue", String.valueOf(mmRequestsQueue.size()));
//                    }
//
//                    try {
//                        sleep(10);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                    if(mmRequestsQueue.size() < 1){
//                        addRequestToQueue(RemoteFunction.IGNORED);
//                    }
//                }
//            }
//        }
//
//        private void cancel(){
//            if(mmRequestsQueue != null){
//                mmRequestsQueue = null;
//            }
//        }
//    }

    private class ResponseDispatchHandler extends Handler{
        private final OutputStream mmOstream;
        private final ConcurrentLinkedQueue<RemoteFunction> mmRequestsQueue;
        private  ScheduledExecutorService mmScheduledThreadPoolExecutor;
        private static final int WRITE_DELAY_MS= 500;

        ResponseDispatchHandler(Looper looper, OutputStream outputStream, ConcurrentLinkedQueue<RemoteFunction> queue){
            super(looper);
            mmOstream = outputStream;
            mmRequestsQueue = queue;
        }

        @Override
        public void handleMessage(Message msg) {
            if(!mmRequestsQueue.isEmpty()){
                int code = mmRequestsQueue.peek().code();
                if(code == RemoteFunction.MADM.code()){
                    try {
                        writeFirstFrame();
                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                    mmRequestsQueue.remove();
                }else if(code == RemoteFunction.START_SENDING.code()){
                    startSending();
                }else if(code == RemoteFunction.STOP_SENDING.code()){
                    stopSending();
                }else{
                    RemoteFunction remoteFunction = mmRequestsQueue.peek();
                    try {
                        write(remoteFunction);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mmRequestsQueue.remove();
                }
            }
        }



        private synchronized void writeFirstFrame() throws IOException {
            ByteArrayOutputStream outputStream;
            outputStream = new ByteArrayOutputStream( );
            outputStream.write((byte) RemoteFunction.MADM.code());
            byte[] madmBytes = RemoteFunction.MADM.message().getBytes();
            outputStream.write(madmBytes, 0, madmBytes.length);

            mmOstream.write(outputStream.toByteArray());
        }

        private synchronized void write(RemoteFunction remoteFunction) throws IOException {

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] timestamp = getTimestamp();
            byte[] remoteFunctionResponse  = getResponse(remoteFunction.code());

            byteArrayOutputStream.write(remoteFunction.code());
            byteArrayOutputStream.write(timestamp,0, timestamp.length);
            if(remoteFunctionResponse != null){
                byteArrayOutputStream.write(remoteFunctionResponse, 0, remoteFunctionResponse.length);
            }

            mmOstream.write(byteArrayOutputStream.toByteArray());
        }

        private void startSending() {
            try {
                write(RemoteFunction.START_SENDING);
            } catch (IOException e) {
                e.printStackTrace();
            }
            mmScheduledThreadPoolExecutor = Executors.newScheduledThreadPool(1);
            mmScheduledThreadPoolExecutor.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    try {
                        write(RemoteFunction.MSG_START_SENDING_FRAMES);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }, 10, 10, TimeUnit.MILLISECONDS);
        }

        private void stopSending(){
            try {
                write(RemoteFunction.STOP_SENDING);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(!mmScheduledThreadPoolExecutor.isShutdown()){
                mmScheduledThreadPoolExecutor.shutdown();
            }
        }
    }
}
