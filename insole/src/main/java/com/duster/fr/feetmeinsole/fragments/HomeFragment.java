package com.duster.fr.feetmeinsole.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;

import com.duster.fr.feetmeinsole.R;
import com.duster.fr.feetmeinsole.managers.IInsoleManager;
import com.duster.fr.feetmeinsole.utils.PreferenceUtils;
import com.duster.fr.feetmeinsole.utils.Utils;
import com.duster.fr.feetmeinsole.views.CircularRippleView;

/**
 * Created by Anas on 13/02/2016.
 */
public class HomeFragment extends ServiceFragment {

    private CircularRippleView mCircularRippleView;
    private ObjectAnimator mRippleAnimator;
    private BroadcastReceiver mReceiver;

    private void registerReceiver(){
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if(action.equals(IInsoleManager.ACTION_BT_CONNECTION)){
                    onBTConnection();
                }else if(action.equals(IInsoleManager.ACTION_BT_DISCONNECTION)){
                    onBTDisconnection();
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(IInsoleManager.ACTION_BT_CONNECTION);
        filter.addAction(IInsoleManager.ACTION_BT_DISCONNECTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mReceiver, filter);
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mCircularRippleView = (CircularRippleView) view.findViewById(R.id.circular_ripple_view);

        FloatingActionButton mAcceptBtn = (FloatingActionButton) view.findViewById(R.id.accept_button);
        FloatingActionButton mStopBtn = (FloatingActionButton) view.findViewById(R.id.stop_button);

        mAcceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMainServiceManager.startConnection();
                int t = PreferenceUtils.getDefaultDiscoverablityTime(getContext())*60*60;
                Utils.makeDiscoverable(getContext(), t);
                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        });

        mStopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMainServiceManager.stopConnection();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReceiver);
    }

    private void startRippleEffect() {

        if(mRippleAnimator != null){
            mRippleAnimator.cancel();
        }

        mCircularRippleView.setInnerCircleRadiusProgress(0);
        mCircularRippleView.setOuterCircleRadiusProgress(0);

        mRippleAnimator = ObjectAnimator.ofFloat(mCircularRippleView,
                CircularRippleView.OUTER_CIRCLE_RADIUS_PROGRESS, 0.1f, 1f);

        mRippleAnimator.setDuration(3000);
        mRippleAnimator.setRepeatCount(ObjectAnimator.INFINITE);
        mRippleAnimator.setRepeatMode(ObjectAnimator.RESTART);
        mRippleAnimator.setInterpolator(new DecelerateInterpolator());
        mRippleAnimator.start();
    }

    private void stopRippleEffect(){
        if(mRippleAnimator != null){
            mRippleAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mRippleAnimator.cancel();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                    mRippleAnimator.cancel();
                }
            });
        }
    }

    @Override
    protected void onServiceConnected() {
//        if(mMainServiceManager.isConnectionStarted()){
//            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//            Utils.makeDiscoverable(getActivity(), PreferenceUtils.getDefaultDiscoverablityTime(getActivity()));
//        }
    }

    private void onBTConnection() {
        if(mMainServiceManager != null){
            startRippleEffect();
        }
    }

    private void onBTDisconnection() {
        if(mMainServiceManager != null){
            stopRippleEffect();
        }
    }
}
