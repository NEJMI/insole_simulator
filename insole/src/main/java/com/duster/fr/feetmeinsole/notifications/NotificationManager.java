package com.duster.fr.feetmeinsole.notifications;

import android.app.Notification;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

/**
 * Created by Anas on 24/02/2016.
 */
public abstract class NotificationManager implements INotificationManager{

    protected Context mContext;

    public NotificationManager(Context context){
        mContext = context;
    }

    protected void sendNotification(String title, String content, int id) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mContext)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(content))
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setDefaults(Notification.DEFAULT_ALL);

        // Gets an instance of the NotificationManager service
        android.app.NotificationManager notifyMgr =
                (android.app.NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        // Builds the notification and issues it.
        notifyMgr.notify(id, mBuilder.build());
    }
}
