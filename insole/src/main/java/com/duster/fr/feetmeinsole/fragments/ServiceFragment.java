package com.duster.fr.feetmeinsole.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.Fragment;

import com.duster.fr.feetmeinsole.service.IMainServiceManager;
import com.duster.fr.feetmeinsole.service.MainService;

/**
 * Created by Anas on 13/02/2016.
 */
public abstract class ServiceFragment extends Fragment {

    protected IMainServiceManager mMainServiceManager;
    private boolean bound;

    @Override
    public void onResume() {
        super.onResume();
        bindService();
    }

    @Override
    public void onPause() {
        super.onPause();
        unbindService();
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder binder) {
            MainService.MainBinder b = (MainService.MainBinder) binder;
            mMainServiceManager = b.getMainManager();
            ServiceFragment.this.onServiceConnected();

        }

        public void onServiceDisconnected(ComponentName className) {
            mMainServiceManager = null;
        }
    };

    protected abstract void onServiceConnected();

    private void bindService(){
        if(!bound){
            Intent service = newServiceIntent();
            getActivity().startService(service);
            getActivity().bindService(service, mConnection, Context.BIND_AUTO_CREATE);
            bound = true;
        }
    }

    private Intent newServiceIntent() {
        return new Intent(getContext().getApplicationContext(), MainService.class);
    }

    private void unbindService(){
        if(bound)
            getActivity().unbindService(mConnection);
        bound = false;
    }

}
