package com.duster.fr.feetmeinsole.managers;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.duster.fr.feetmeinsole.Build;
import com.duster.fr.feetmeinsole.controllers.IRemoteDeviceController;
import com.duster.fr.feetmeinsole.controllers.RemoteDeviceController;
import com.duster.fr.feetmeinsole.greendao.Insole;
import com.duster.fr.feetmeinsole.notifications.DisconnectionNotification;

/**
 * Created by Anas on 19/02/2016.
 */
public class InsoleManager implements IInsoleManager, IRemoteDeviceController.Listener {

    private static final String TAG = InsoleManager.class.getSimpleName();
    private IRemoteDeviceController mInsoleController;

    private Insole mInsole;
    private Context mContext;
    private final DisconnectionNotification mDisconnectionNotificationMgr;

    private boolean isServiceOn = true;

    protected IRemoteDeviceController newInsoleController(Context context, Insole insole,
                                                    IRemoteDeviceController.Listener listener){
        return RemoteDeviceController.newInstance(context, insole, listener);
    }

    public static class Builder {
        private final Context context;
        private final Insole insole;
        private DisconnectionNotification discNotificationMgr;

        public Builder(Context context, Insole insole) {
            this.context = context;
            this.insole = insole;
        }

        public Builder setDiscNotificationManager(DisconnectionNotification val) {
            this.discNotificationMgr = val;
            return this;
        }

        public InsoleManager build() {
            return new InsoleManager(this);
        }
    }

    protected InsoleManager(Builder builder){

        mContext = builder.context;
        mInsole = builder.insole;
        mDisconnectionNotificationMgr = builder.discNotificationMgr;

        if(mContext == null) throw  new IllegalStateException("Context cannot be null");
        if(mInsole == null) throw  new IllegalStateException("Insole cannot be null");
    }

    @Override
    public void startAcceptingConnections() {
        if(Build.DEBUG) Log.d(TAG, "start accept thread " + mInsole.getSide());
        if(mInsoleController == null) {
            mInsoleController = newInsoleController(mContext, mInsole, this);
        }
        int state = mInsoleController.getState();
        if(state == IRemoteDeviceController.STATE_DISCONNECTED) {
            mInsoleController.accept();
        }
    }

    @Override
    public void stopAcceptingConnections() {
        isServiceOn = false;
        if(mInsoleController != null){
            mInsoleController.disconnect();
            mInsoleController = null;
        }
    }

    @Override
    public boolean isConnected() {
        return mInsoleController != null
                && mInsoleController.getState() == IRemoteDeviceController.STATE_CONNECTED;
    }


    @Override
    public void onStateChanged(int state) {
        if(state == IRemoteDeviceController.STATE_CONNECTED){
            onConnection();
        }else if(state == IRemoteDeviceController.STATE_DISCONNECTED){
            onDisconnection();
        }
    }

    private void onConnection() {
        broadcastConnectionChange(mInsole.getSide(), true);
        if (mInsoleController != null){
            mInsoleController.sendFirstFrame();
        }

        if(mDisconnectionNotificationMgr != null) mDisconnectionNotificationMgr.prepareNotification();
    }

    private void onDisconnection() {
        broadcastConnectionChange(mInsole.getSide(), false);

        if(isServiceOn && mDisconnectionNotificationMgr != null) mDisconnectionNotificationMgr.sendNotification();
    }

    private void broadcastConnectionChange(int side, boolean connected){
        Intent intent = new Intent();
        intent.setAction(connected ? ACTION_BT_CONNECTION : ACTION_BT_DISCONNECTION);
        intent.putExtra(EXTRA_SIDE, side);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }


    @Override
    public void onBatteryRequested() {
        //TODO
    }

    @Override
    public void onVersionRequested() {
        //TODO
    }

    @Override
    public void onSensorNbRequested() {
        //TODO
    }

    @Override
    public void onSizeRequested() {
        //TODO
    }

    @Override
    public void onSideRequested() {
        //TODO
    }

    @Override
    public void onPongRequested() {
        //TODO
    }

    @Override
    public void onResetTimestampRequested() {
        //TODO
    }

    @Override
    public void onStartSending() {
        //TODO
    }

    @Override
    public void onStopSending() {
        stopAcceptingConnections();
    }

    @Override
    public void onCalibration() {
        //TODO
    }

    @Override
    public void onRemoteDeviceConnected(BluetoothDevice device) {
        if(device != null){
            Intent intent = new Intent();
            intent.setAction(ACTION_REMOTE_DEVICE);
            intent.putExtra(EXTRA_REMOTE_DEVICE_NAME, device.getName());
            intent.putExtra(EXTRA_REMOTE_DEVICE_ADRESS, device.getAddress());
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        }
    }
}
